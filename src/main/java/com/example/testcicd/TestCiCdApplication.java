package com.example.testcicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class TestCiCdApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestCiCdApplication.class, args);
    }


    @PostMapping("/test")
    public String test(@RequestBody Object object){
        System.out.println(object.toString());
        String toString = object.toString();
        return "Jenkinasnbxcvghsvxhs test 2 ci cd gitlab test";
    }
}
